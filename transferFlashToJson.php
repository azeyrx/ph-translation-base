<?php

$langJson = file_get_contents('ext.json');
$deText = file_get_contents('de_ext.txt');

$decodedLangJson = json_decode($langJson, true);
$explodedDeLang = explode("\r\n", $deText);

foreach($explodedDeLang as $sourceRow){
    $toKeyValuePair = explode('=', $sourceRow);

    if(array_key_exists($toKeyValuePair[0], $decodedLangJson)){
        $decodedLangJson[$toKeyValuePair[0]] = $toKeyValuePair[1];
    }
}

echo json_encode($decodedLangJson, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
exit;
